import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-onepark',
  templateUrl: './onepark.page.html',
  styleUrls: ['./onepark.page.scss'],
})
export class OneparkPage implements OnInit {

  name: string;
  maxCapacity: number;
  spacesAvailable: number;
  carParkId: number;
  text: string;

  constructor(public router: Router, public activatedRoute: ActivatedRoute, public navCtrl: NavController, private location: Location) {}

  ngOnInit() {
    this.carParkId = Number(this.activatedRoute.snapshot.paramMap.get('carParkId'));
    this.name = this.activatedRoute.snapshot.paramMap.get('name');
    this.spacesAvailable = Number(this.activatedRoute.snapshot.paramMap.get('spacesAvailable'));
    this.maxCapacity = Number(this.activatedRoute.snapshot.paramMap.get('maxCapacity'));
  }  
  goBack() {
    this.location.back();
  }
  go_map() {
    this.router.navigateByUrl('/tabs/map');
  }
}
