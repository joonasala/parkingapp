import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OneparkPage } from './onepark.page';

describe('OneparkPage', () => {
  let component: OneparkPage;
  let fixture: ComponentFixture<OneparkPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OneparkPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OneparkPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
