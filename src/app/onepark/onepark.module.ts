import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import { OneparkPageRoutingModule } from './onepark-routing.module';
import { OneparkPage } from './onepark.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OneparkPageRoutingModule
  ],
  declarations: [OneparkPage]
})
export class OneparkPageModule {}
