import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OneparkPage } from './onepark.page';

const routes: Routes = [
  {
    path: '',
    component: OneparkPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OneparkPageRoutingModule {}
