import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { ApolloQueryResult } from 'apollo-client';

@Component({
  selector: 'app-allparks',
  templateUrl: './allparks.page.html',
  styleUrls: ['./allparks.page.scss'],
})
export class AllparksPage implements OnInit {

  constructor(private apollo: Apollo, public router: Router) {}
  
  carParks: any[];
  carParkId: Number;
  name: string;
  
  ngOnInit() {
    this.apollo
      .watchQuery({
        query: gql`
          {
            carParks {
              carParkId
              name
              lat
              lon
              maxCapacity
              spacesAvailable
            }
          }
        `,
      })
      .valueChanges.subscribe((result : ApolloQueryResult<any> ) => {
        this.carParks = result.data && result.data.carParks;
      });
  }
}
