import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AllparksPageRoutingModule } from './allparks-routing.module';

import { AllparksPage } from './allparks.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AllparksPageRoutingModule
  ],
  declarations: [AllparksPage]
})
export class AllparksPageModule {}
