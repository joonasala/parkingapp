import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AllparksPage } from './allparks.page';

describe('AllparksPage', () => {
  let component: AllparksPage;
  let fixture: ComponentFixture<AllparksPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllparksPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AllparksPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
