import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-info',
  templateUrl: './info.page.html',
  styleUrls: ['./info.page.scss'],
})
export class InfoPage implements OnInit {

  constructor(public router: Router) { }

  ngOnInit() {
  }
  go_home() {
    this.router.navigateByUrl('home');
  }
  all_parks() {
    this.router.navigateByUrl('allparks');
  }
  go_map(){
    this.router.navigateByUrl('map');
  }
}
