import { Component, AfterViewInit, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { Geolocation } from '@ionic-native/geolocation/ngx';
declare var google;

@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit, AfterViewInit {
  latitude: any;
  longitude: any;
  map;

  @ViewChild('mapElement', {static: true}) mapNativeElement: ElementRef;

  constructor(public router: Router, private geolocation: Geolocation) {}

  ngOnInit() {
  }
  ngAfterViewInit(): void {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.latitude = resp.coords.latitude;
      this.longitude = resp.coords.longitude;
      const map = new google.maps.Map(this.mapNativeElement.nativeElement, {
        center: {lat: 65.010042, lng: 25.487214}, // Kartan aloitus
        zoom: 13
      });
       
      const userPos = {  //käyttäjän sijainti
        lat: this.latitude,
        lng: this.longitude
      };
      const userIcon = {
      url:'assets/icon/mapUser3.png',
      scaledSize: new google.maps.Size(20,20),
      };

      const parkIcon = {
        url:'assets/icon/mapPark.png',
        scaledSize: new google.maps.Size(25,25),
        };
     
      var lastWindow=null;
    
      // Karttamerkit
      const userMarker = new google.maps.Marker({ 
        position: userPos,
        map: map,
        title: 'Käyttäjä',
        icon: userIcon
      });
      const markerAutoTori = new google.maps.Marker({ 
        position: {lat: 65.011090, lng: 25.465013},
        map: map,
        title: 'Autotori',
        icon: parkIcon
      });
      const markerReha = new google.maps.Marker({   
        position: {lat: 65.010954, lng: 25.512384},
        map: map,
        title: 'Rehaparkki',
        icon: parkIcon
      });
      const markerAvohoito = new google.maps.Marker({   
        position: {lat: 65.006811, lng: 25.513953},
        map: map,
        title: 'Avohoitotalo',
        icon: parkIcon
      });
      const markerTechnopolis = new google.maps.Marker({   
        position: {lat: 65.007767, lng: 25.468563},
        map: map,
        title: 'Technopolis',
        icon: parkIcon
      });
      const markerKiviUusikatu = new google.maps.Marker({    
        position: {lat: 65.012314, lng: 25.476454},
        map: map,
        title: 'Kivisydän, Hallituskadun ramppi',
        icon: parkIcon
      });
      const markerKiviTorinranta = new google.maps.Marker({  
        position: {lat: 65.015214, lng: 25.465782},
        map: map,
        title: 'Kivisydän, Torinrannan ramppi',
        icon: parkIcon
      });
      const markerAutoSaari = new google.maps.Marker({   
        position: {lat: 65.010907, lng: 25.473442},
        map: map,
        title: 'Autosaari',
        icon: parkIcon
      });
      const markerAutoheikki = new google.maps.Marker({    
        position: {lat: 65.014950, lng: 25.478950},
        map: map,
        title: 'Autoheikki',
        icon: parkIcon
      });
      const markerSairaalaparkki = new google.maps.Marker({    
        position: {lat: 65.009200, lng: 25.513159},
        map: map,
        title: 'Sairaalaparkki',
        icon: parkIcon
      });
      const markerValkea = new google.maps.Marker({    
        position: {lat: 65.010569, lng: 25.471007},
        map: map,
        title: 'Valkea',
        icon: parkIcon
      });

      const userInfo =   
       '<div>Olet tässä.</div>'

    const infoWindowUser = new google.maps.InfoWindow({ 
    content: userInfo,
    maxwidth: 400
    });
   
    userMarker.addListener('click', function() { 
      infoWindowUser.open(map, userMarker)
    });

    markerAutoTori.addListener('click', function() {
      if (lastWindow) lastWindow.close();
      infoWindow0.open(map, markerAutoTori);
      lastWindow = infoWindow0;
    });
    markerReha.addListener('click', function() {
      if (lastWindow) lastWindow.close();
      infoWindow1.open(map, markerReha);
      lastWindow = infoWindow1;
    });
    markerAvohoito.addListener('click', function() {
      if (lastWindow) lastWindow.close();
      infoWindow2.open(map, markerAvohoito);
      lastWindow = infoWindow2;
    });
    markerTechnopolis.addListener('click', function() {
      if (lastWindow) lastWindow.close();
      infoWindow3.open(map, markerTechnopolis);
      lastWindow = infoWindow3;
    });
    markerKiviUusikatu.addListener('click', function() {
      if (lastWindow) lastWindow.close();
      infoWindow4.open(map, markerKiviUusikatu);
      lastWindow = infoWindow4;
    });
    markerKiviTorinranta.addListener('click', function() {
      if (lastWindow) lastWindow.close();
      infoWindow5.open(map, markerKiviTorinranta);
      lastWindow = infoWindow5;
    });
    markerAutoSaari.addListener('click', function() {
      if (lastWindow) lastWindow.close();
      infoWindow6.open(map, markerAutoSaari);
      lastWindow = infoWindow6;
    });
    markerAutoheikki.addListener('click', function() {
      if (lastWindow) lastWindow.close();
      infoWindow7.open(map, markerAutoheikki);
      lastWindow = infoWindow7;
    });
    markerSairaalaparkki.addListener('click', function() {
      if (lastWindow) lastWindow.close();
      infoWindow8.open(map, markerSairaalaparkki);
      lastWindow = infoWindow8;
    });
    markerValkea.addListener('click', function() {
      if (lastWindow) lastWindow.close();
      infoWindow9.open(map,  markerValkea);
      lastWindow = infoWindow9;
    });
    
    
    var contentString0 =   
    '<div>Autotori, Albertinkatu 3</div>'
   ;
   var contentString1 =   
    '<div>Rehaparkki, Kiviharjunlenkki 8</div>'
   ;
   var contentString2 =   
    '<div>Avohoitotalo, Kiviharjuntie 9</div>'
   ;
   var contentString3 =   
    '<div>Technopolis, Nummikatu 35</div>'
   ;
   var contentString4 =   
    '<div>Kivisydän, Hallituskadun ramppi, Hallituskatu 20</div>'
   ;
   var contentString5 =   
    '<div>Kivisydän, Torinrannan ramppi, Hallituskatu 1</div>'
   ;
   var contentString6 =   
    '<div>Autosaari, Kauppurienkatu 24</div>'
   ;
   var contentString7 =   
    '<div>Autoheikki, Heikinkatu 5</div>'
   ;
   var contentString8 =   
    '<div>Sairaalaparkki, Kiviharjuntie 5</div>'
   ;
   var contentString9 =   
    '<div>Valkea,  Isokatu 25</div>'
   ;

    var infoWindow0 = new google.maps.InfoWindow({
      content: contentString0,
      maxwidth: 400
    });
    var infoWindow1 = new google.maps.InfoWindow({
      content: contentString1,
      maxwidth: 400
    });
    var infoWindow2 = new google.maps.InfoWindow({
      content: contentString2,
      maxwidth: 400
    });
    var infoWindow3 = new google.maps.InfoWindow({
      content: contentString3,
      maxwidth: 400
    });
    var infoWindow4 = new google.maps.InfoWindow({
      content: contentString4,
      maxwidth: 400
    });
    var infoWindow5 = new google.maps.InfoWindow({
      content: contentString5,
      maxwidth: 400
    });
    var infoWindow6 = new google.maps.InfoWindow({
      content: contentString6,
      maxwidth: 400
    });
    var infoWindow7 = new google.maps.InfoWindow({
      content: contentString7,
      maxwidth: 400
    });
    var infoWindow8 = new google.maps.InfoWindow({
      content: contentString8,
      maxwidth: 400
    });
    var infoWindow9 = new google.maps.InfoWindow({
      content: contentString9,
      maxwidth: 400
    });

    }).catch((error) => {
      console.log('Sijainnin paikallistaminen ei onnistunut.', error);
    });

  }
}
