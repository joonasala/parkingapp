import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { ApolloQueryResult } from 'apollo-client';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  carParks: any[];
  

  constructor(private apollo: Apollo, public router: Router) {}
  
  ngOnInit() {
    this.apollo
      .watchQuery({
        query: gql`
          {
            carParks {
              carParkId
              name
              lat
              lon
              maxCapacity
              spacesAvailable
            }
          }
        `,
      })
      .valueChanges.subscribe((result : ApolloQueryResult<any> ) => {
        this.carParks = result.data && result.data.carParks;
        
      });
  }
  all_parks() {
    this.router.navigateByUrl('tabs/allparks');
  }
}





