import { browser, by, element } from 'protractor';
import { protractor } from 'protractor/built/ptor';

export class AppPage {
  navigateTo(url: string = 'tabs/home') {
    return browser.get(url);
  }

  waitPageToLoad(page: string) {
    browser.wait(protractor.ExpectedConditions.urlContains(page), 5000);
  }
  
  clickElement(selector: string) {
    return new Promise((resolve) => {
      element(by.css(selector)).click().then(() => {
      resolve();
    });
  });
  }

  

  getParagraphText() {
    return element(by.deepCss('app-root ion-content')).getText();
  }
}
