import { AppPage } from './app.po';

describe('new App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it ('should navigate to ...', () => {
    page.navigateTo('tabs/allparks');
    page.waitPageToLoad('tabs/allparks');
    page.clickElement('ion-card').then(() => {
      expect(page.navigateTo('tabs/onepark'));
    //  page.waitPageToLoad('tabs/onepark');
    });
  });
});
